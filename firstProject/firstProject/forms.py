import re

from django import forms

from ckeditor.widgets import CKEditorWidget
from transliterate import translit
from transliterate.exceptions import LanguageDetectionError

from firstProject import models


def transliterate(text):
    pieces = str(re.sub('[\W]+', ' ', text)).lower().split(' ')
    result = []

    for piece in pieces:
        try:
            result.append(translit(piece, reversed=True))
        except LanguageDetectionError:
            result.append(piece)
    return '-'.join([r for r in result if r])

class Youtube_videoForm(forms.modelForm):
    class Meta:
        model = models.Youtube_video
        fields = '__all__'


class ArticleForm(forms.modelForm):
    class Meta:
        model = models.Article
        fields = '__all__'

class bank_cardForm(forms.modelForm):
    class Meta:
        model = models.bank_card
        fields = '__all__'
