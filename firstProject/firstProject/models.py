from transliterate.utils import _

from easynote import settings
from .models import  BaseModel
from django.contrib.auth.models import User
from django.db import models

user = User.objects.create_user('alexa', 'aleksandranecheporuk@gmail.com', 'behindblueeyes')

# Обновите поля и сохраните их снова
user.first_name = 'Alexa'
user.last_name = 'Bilosh'
user.save()


class BaseModel (BaseModel):
    created_at = models.datetime


class Article(BaseModel):
    title = models. Charfield(
        max_length=100,
        unique=True,
        verbose_name=_('Title')
    )

class Youtube_video (BaseModel):
    video = models.videoField(
        blank=True,
        verbose_name =_('Video'),
        null=True
    )
class bank_card (BaseModel):
    card = models.NumberField(
    Max_length = 12,
    null = False,
    unique = True
    )

class ContentItem(models.Model):
    author = models.ForeignKey(

        verbose_name=_('Author'),
        related_name='contentItem',
        on_delete=models.SET_NULL,
        to=settings.AUTH_USER_MODEL,
        null=True
    )