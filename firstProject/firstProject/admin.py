from django.contrib import admin
import firstProject
from firstProject import forms

@admin.register(firstProject.models.Article)
class ArticleAdmin(admin.ModelAdmin):
    form = forms.ArticleForm

@admin.register(firstProject.models.Youtube_video)
class ArticleAdmin(admin.ModelAdmin):
    form = forms.Youtube_video

@admin.register(firstProject.models.bank_card)
class ArticleAdmin(admin.ModelAdmin):
    form = forms.bank_card